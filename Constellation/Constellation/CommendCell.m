//
//  CommendCell.m
//  Constellation
//
//  Created by Darren  xu on 16/6/30.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "CommendCell.h"

@implementation CommendCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _headImg.layer.cornerRadius = 5;
    _headImg.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
