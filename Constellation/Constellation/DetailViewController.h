//
//  DetailViewController.h
//  Constellation
//
//  Created by Darren  xu on 16/7/1.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *starImage;
@property (weak, nonatomic) IBOutlet UILabel *starLabel;
@property (nonatomic, copy) NSString *starNum;

- (IBAction)goBack:(UIBarButtonItem *)sender;

@end
