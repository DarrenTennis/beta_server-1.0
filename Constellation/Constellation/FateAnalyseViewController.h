//
//  FateAnalyseViewController.h
//  Constellation
//
//  Created by Darren  xu on 16/6/27.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FateAnalyseViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *leftArrow;

- (IBAction)goBack:(UIBarButtonItem *)sender;
- (IBAction)share:(UIBarButtonItem *)sender;
@end
