//
//  commendUserModel.m
//  Constellation
//
//  Created by Darren  xu on 16/7/1.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "commendUserModel.h"

@implementation commendUserModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    NSLog(@"value=%@ , key=%@", value, key);
    if ([key isEqualToString:@"description"])
    {
        [self setValue:value forKey:@"userDescription"];
    }
}

@end
