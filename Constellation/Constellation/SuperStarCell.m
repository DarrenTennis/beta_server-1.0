//
//  SuperStarCell.m
//  Constellation
//
//  Created by Darren  xu on 16/7/1.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "SuperStarCell.h"

@implementation SuperStarCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _headImg.layer.cornerRadius = 5;
    _headImg.layer.masksToBounds = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
