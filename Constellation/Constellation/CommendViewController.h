//
//  CommendViewController.h
//  Constellation
//
//  Created by Darren  xu on 16/6/30.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommendViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tab;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

- (IBAction)leftBtn:(UIBarButtonItem *)sender;
- (IBAction)rightBtn:(UIBarButtonItem *)sender;
@end
