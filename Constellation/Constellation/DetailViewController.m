//
//  DetailViewController.m
//  Constellation
//
//  Created by Darren  xu on 16/7/1.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _starLabel.preferredMaxLayoutWidth = DEVICE_WIDTH - 60;
    _starLabel.text = [self getContent];   // 用table来做，才能滑动
    _starImage.image = [self getStarImage];
    
}

// 将状态栏文字设置为白色
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (NSString *)getContent{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"star_baike" ofType:@"dat"];
    NSString *allContent = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];

    NSArray *arr = [allContent componentsSeparatedByString:@"<star>"];
//    NSLog(@"satr Array %ld",arr.count);
//    for (NSString *ss in arr) {
//        NSLog(@"--------%@",ss);
//    }
    int num = [_starNum intValue];
    NSString *tempStr = arr[num];
    NSString *content = [tempStr stringByReplacingOccurrencesOfString:@"</star>" withString:@""];
    return content;
}

- (UIImage *)getStarImage{
    return [UIImage imageNamed:[NSString stringWithFormat:@"bg_me_%@",_starNum]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)goBack:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
