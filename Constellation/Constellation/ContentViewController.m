//
//  ContentViewController.m
//  Constellation
//
//  Created by Darren  xu on 16/6/27.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "ContentViewController.h"
#import "FateAnalyseViewCell.h"
#import "FateAnalyseViewCell2.h"
#import "TitleViewCell.h"
#import "DetailViewController.h"

@interface ContentViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) ContentModel_VC1 *model;
@property (nonatomic, strong) NSMutableArray *starNameArr;  // 星座的中文名称数组

@end

@implementation ContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_tab registerNib:[UINib nibWithNibName:@"FateAnalyseViewCell" bundle:nil] forCellReuseIdentifier:@"fateCell"];
    [_tab registerNib:[UINib nibWithNibName:@"FateAnalyseViewCell2" bundle:nil] forCellReuseIdentifier:@"fateCell2"];
    [_tab registerNib:[UINib nibWithNibName:@"TitleViewCell" bundle:nil] forCellReuseIdentifier:@"titleCell"];
    
    _tab.estimatedRowHeight = 100;
    _tab.rowHeight = UITableViewAutomaticDimension;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        NSLog(@"下拉刷新");
        // block重新请求数据
        [self performSelector:@selector(refreshTableData) withObject:nil afterDelay:2];
    }];
    _tab.mj_header = header;

//    _tab.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        NSLog(@"下拉刷新");
//        // block重新请求数据
//        [self performSelector:@selector(refreshTableData) withObject:nil afterDelay:2];
//    }];
    
    
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    _tab.mj_header.automaticallyChangeAlpha = YES;
    // 隐藏时间
    //header.lastUpdatedTimeLabel.hidden = YES;
    // 隐藏状态
    //header.stateLabel.hidden = YES;
    
//    [header setTitle:@"使劲拉" forState:MJRefreshStateIdle];
//    [header setTitle:@"该放手了" forState:MJRefreshStatePulling];
//    [header setTitle:@"刷新中" forState:MJRefreshStateRefreshing];
    
    
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = nil;
    if (indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"titleCell" forIndexPath:indexPath];
        TitleViewCell *titleCell = (TitleViewCell *)cell;
        if (_starNum) {
            int num = [_starNum intValue];
            titleCell.nameLab.text = [NSString stringWithFormat:@"%@ (%@)",self.starNameArr[num-1],_contentDic[@"time"]];
        }
        
    }else if (indexPath.row == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"fateCell" forIndexPath:indexPath];
        FateAnalyseViewCell *cell1 = (FateAnalyseViewCell *)cell;
        
        cell1.block = ^(){
            NSLog(@"用block实现————点击单元格中的按钮 跳转界面");
            DetailViewController *detailVC = [[DetailViewController alloc]init];
            detailVC.starNum = self.starNum;
            [self.navigationController pushViewController:detailVC animated:YES];
        };
        
        int starNumber1 = [_contentDic[@"luck_value"] intValue];
        int starNumber2 = [_contentDic[@"love_value"] intValue];
        int starNumber3 = [_contentDic[@"fortune_value"] intValue];
        int starNumber4 = [_contentDic[@"work_value"] intValue];
        
        cell1.image1.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_star%d",starNumber1]];
        cell1.image2.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_star%d",starNumber2]];
        cell1.image3.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_star%d",starNumber3]];
        cell1.image4.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_star%d",starNumber4]];
        
        if (_starNum) {
            int num = [_starNum intValue];
            cell1.imageBig.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_trendstar%d",num]];
        }
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"fateCell2" forIndexPath:indexPath];
        FateAnalyseViewCell2 *cell2 = (FateAnalyseViewCell2 *)cell;
        cell2.fateContent.text = _contentDic[@"trend"];
    }
    
    return cell;
}

- (void)refreshTableData{
    [self.tab.mj_header endRefreshing];
    [_tab reloadData];
}

- (void)loadScrollContent:(ContentModel_VC1 *)model{
    self.model = model;
    [_tab reloadData];
}


- (NSMutableArray *)starNameArr{
    if (_starNameArr == nil) {
        _starNameArr = [[NSMutableArray alloc]init];
        [_starNameArr addObject:@"白羊座"];
        [_starNameArr addObject:@"金牛座"];
        [_starNameArr addObject:@"双子座"];
        [_starNameArr addObject:@"巨蟹座"];
        [_starNameArr addObject:@"狮子座"];
        [_starNameArr addObject:@"处女座"];
        [_starNameArr addObject:@"天秤座"];
        [_starNameArr addObject:@"天蝎座"];
        [_starNameArr addObject:@"射手座"];
        [_starNameArr addObject:@"摩羯座"];
        [_starNameArr addObject:@"水瓶座"];
        [_starNameArr addObject:@"双鱼座"];
    }
    return _starNameArr;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
