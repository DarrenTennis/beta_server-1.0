//
//  SearchResultController.h
//  Constellation
//
//  Created by Darren  xu on 16/7/1.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tab;
@property (nonatomic, strong) NSMutableArray *resultArr;

- (IBAction)goBack:(UIBarButtonItem *)sender;
@end
