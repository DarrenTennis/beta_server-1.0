//
//  commendUserModel.h
//  Constellation
//
//  Created by Darren  xu on 16/7/1.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface commendUserModel : NSObject
//"star": "11",
//"sex": "2",
//"birth": "886867200",
//"province": "41",
//"city": "15",
//"nick_name": "李李樱子",
//"description": "深爱不岂容颜",
//"remark": "",
//"pk_count": "2",
//"favorite": "0",
//"type": "1",
//"uid": "3840923406",
//"weibo_name": "李李樱子",
//"icon":
@property (nonatomic, copy) NSString *star;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *birth;
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *nick_name;
@property (nonatomic, copy) NSString *userDescription;
@property (nonatomic, copy) NSString *remark;
@property (nonatomic, copy) NSString *pk_count;
@property (nonatomic, copy) NSString *favorite;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *weibo_name;
@property (nonatomic, copy) NSString *icon;

@end
