//
//  MyTabBarController.m
//  Constellation
//
//  Created by Darren  xu on 16/6/24.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "MyTabBarController.h"
#import "ViewController1.h"
#import "ViewController2.h"
#import "ViewController3.h"
//#import "ViewController4.h"
#import "PartnershipViewController.h"   // 替换VC4
#import "ViewController5.h"

@interface MyTabBarController ()

@end

@implementation MyTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"________新建tabbar");
    self.tabBar.backgroundImage = [UIImage imageNamed:@"navbar_bg"];
//  压栈时隐藏tabbar
    self.hidesBottomBarWhenPushed = YES;
    
    ViewController1 *controller1 = [[ViewController1 alloc]init];
//    controller1.title = @"星座大师";
    ViewController2 *controller2 = [[ViewController2 alloc]init];
//    controller2.title = @"星盘合盘";
    ViewController3 *controller3 = [[ViewController3 alloc]init];
//    controller3.title = @"星友PK";
//    ViewController4 *controller4 = [[ViewController4 alloc]init];
    PartnershipViewController *controller4 = [[PartnershipViewController alloc]init];
//    controller4.title = @"星座配对";
    ViewController5 *controller5 = [[ViewController5 alloc]init];
//    controller4.title = @"更多";

    UITabBarItem *item1 = [[UITabBarItem alloc]initWithTitle:nil image:[[UIImage imageNamed:@"tabBar_n_1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"tabBar_select_n_1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    UITabBarItem *item2 = [[UITabBarItem alloc]initWithTitle:nil image:[[UIImage imageNamed:@"tabBar_n_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"tabBar_select_n_2"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    UITabBarItem *item3 = [[UITabBarItem alloc]initWithTitle:nil image:[[UIImage imageNamed:@"tabBar_n_3"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"tabBar_select_n_3"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    UITabBarItem *item4 = [[UITabBarItem alloc]initWithTitle:nil image:[[UIImage imageNamed:@"tabBar_n_4"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"tabBar_select_n_4"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    UITabBarItem *item5 = [[UITabBarItem alloc]initWithTitle:nil image:[[UIImage imageNamed:@"tabBar_n_5"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"tabBar_select_n_5"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    controller1.tabBarItem = item1;
    controller2.tabBarItem = item2;
    controller3.tabBarItem = item3;
    controller4.tabBarItem = item4;
    controller5.tabBarItem = item5;
    
    self.viewControllers = @[controller1,controller2,controller3,controller4,controller5];
    self.selectedIndex = 0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
