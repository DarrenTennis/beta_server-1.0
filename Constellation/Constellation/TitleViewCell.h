//
//  TitleViewCell.h
//  Constellation
//
//  Created by Darren  xu on 16/6/30.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TitleViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLab;

@end
