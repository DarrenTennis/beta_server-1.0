//
//  SuperStarViewController.h
//  Constellation
//
//  Created by Darren  xu on 16/7/1.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuperStarViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tab;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

- (IBAction)goBasck:(UIBarButtonItem *)sender;
@end
