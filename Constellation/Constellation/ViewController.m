//
//  ViewController.m
//  Constellation
//
//  Created by Darren  xu on 16/6/24.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "ViewController.h"
#import "MyTabBarController.h"

@interface ViewController ()
@property (nonatomic, strong) UIImageView *launchImageView;
@property (nonatomic, strong) MyTabBarController *tabBar;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setUpLaunchImage];
    [self performSelector:@selector(transitionAnimate) withObject:nil afterDelay:1];
    
}

- (void)transitionAnimate
{
    [UIView animateWithDuration:0.5 animations:^{
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.view cache:NO];
    } completion:^(BOOL finished) {
        //跳转到导航
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:self.tabBar];
        nav.navigationBarHidden = YES;
        [[UIApplication sharedApplication].delegate.window setRootViewController:nav];

    }];
}

- (void)setUpLaunchImage
{
    _launchImageView = [UIImageView new];
    _launchImageView.image = [UIImage imageNamed:@"welcome.jpg"];
    [self.view addSubview:_launchImageView];
    
    // 使用Masonry添加约束
    [_launchImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

- (MyTabBarController *)tabBar{
    if (_tabBar == nil) {
        _tabBar = [[MyTabBarController alloc]init];
    }
    return _tabBar;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
