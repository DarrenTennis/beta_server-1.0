//
//  PairedResultViewController.h
//  Constellation
//
//  Created by mac on 16/6/30.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PairedResultViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tabview;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewLeft;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewRight;
@property (weak, nonatomic) IBOutlet UILabel *leftconstellationLab;
@property (weak, nonatomic) IBOutlet UILabel *leftsexLab;
@property (weak, nonatomic) IBOutlet UILabel *rightconstellationLab;
@property (weak, nonatomic) IBOutlet UILabel *rightsexLab;
- (IBAction)back:(UIButton *)sender;

@end
