//
//  SearchResultController.m
//  Constellation
//
//  Created by Darren  xu on 16/7/1.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "SearchResultController.h"
#import "CommendCell.h"
#import "commendUserModel.h"

@interface SearchResultController ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation SearchResultController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_tab registerNib:[UINib nibWithNibName:@"CommendCell" bundle:nil] forCellReuseIdentifier:@"commendCell"];
    _tab.rowHeight = 61;
}

// 将状态栏文字设置为白色
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _resultArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CommendCell *cell = [tableView dequeueReusableCellWithIdentifier:@"commendCell" forIndexPath:indexPath];
    commendUserModel *user = _resultArr[indexPath.row];
    
    cell.nameLab.text = user.nick_name;
    if (user.icon) {
        [cell.headImg setImageWithURL:[NSURL URLWithString:user.icon]];
    }else{
        cell.headImg.image = [UIImage imageNamed:@"bg_head_placeholder"];
    }
    if ([user.sex isEqualToString:@"1"]) {
        cell.sexImg.image = [UIImage imageNamed:@"bg_show_male"];
    }else{
        cell.sexImg.image = [UIImage imageNamed:@"bg_show_female"];
    }
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)goBack:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
