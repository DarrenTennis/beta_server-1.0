//
//  PairedResultViewController.m
//  Constellation
//
//  Created by mac on 16/6/30.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "PairedResultViewController.h"
#import "OneTableViewCell.h"
#import <AFNetworking.h>
#import "TwoTableViewCell.h"
@interface PairedResultViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,retain) NSMutableDictionary *dic;
@end

@implementation PairedResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tabview.estimatedRowHeight = 66;
    _tabview.rowHeight = UITableViewAutomaticDimension;
    // Do any additional setup after loading the view from its nib.
    AFHTTPSessionManager *manager=[AFHTTPSessionManager manager];
    [manager GET:@"http://star21.mooker.cn/star/api/v2/getPairInfo.php?star2=6&dev=iphone&devid=rmloU7F9J9F7TtN1lSEspelUyRdkJvFGl3Q6vqHZaPk%3D&locale=0&sex2=f&appid=ff808081347ecaad013492cc2e5c0003&ver=3.1&star1=5&sex1=m" parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"----%@",responseObject);
        _dic=responseObject;
        [self.tabview reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
    }];
    [_tabview registerNib:[UINib nibWithNibName:@"OneTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell1"];
    [_tabview registerNib:[UINib nibWithNibName:@"TwoTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell2"];
    _tabview.backgroundColor=[UIColor clearColor];
    [_tabview reloadData];
    
    
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *cell = nil;
    if (indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell1" forIndexPath:indexPath];
        OneTableViewCell *cell1 = (OneTableViewCell *)cell;
        cell1.backgroundColor=[UIColor clearColor];
        cell1.zhiShu.text= [NSString stringWithFormat:@"爱情配对指数%@",_dic[@"data"][@"pair_value"]];
        cell1.zhiShu.textColor=[UIColor whiteColor];
        cell1.biZhong.text=[NSString stringWithFormat:@"星座比重%@",_dic[@"data"][@"rate"]];
        cell1.biZhong.textColor=[UIColor whiteColor];
        
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell2" forIndexPath:indexPath];
        TwoTableViewCell *cell2 = (TwoTableViewCell *)cell;
        cell2.backgroundColor=[UIColor clearColor];
        cell2.lab.text=_dic[@"data"][@"info"];
        cell2.lab.textColor=[UIColor whiteColor];
    }
    
    return cell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
    
    
    
}
@end
