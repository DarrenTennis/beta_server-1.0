//
//  FriendViewCell.h
//  Constellation
//
//  Created by Darren  xu on 16/6/29.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *groupLab;
@property (weak, nonatomic) IBOutlet UILabel *numLab;
@property (weak, nonatomic) IBOutlet UIImageView *narrowImg;

@end
