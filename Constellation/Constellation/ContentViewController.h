//
//  ContentViewController.h
//  Constellation
//
//  Created by Darren  xu on 16/6/27.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContentModel_VC1.h"

@interface ContentViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tab;
@property (nonatomic, strong) NSMutableDictionary *contentDic;   //星座运势具体内容
@property (nonatomic, strong) NSString *starNum;  //当前的星座序号

- (void)loadScrollContent:(ContentModel_VC1 *)model;
- (void)refreshTableData;

@end
