//
//  TwoTableViewCell.h
//  Constellation
//
//  Created by mac on 16/7/1.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TwoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lab;

@end
