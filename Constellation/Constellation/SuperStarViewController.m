//
//  SuperStarViewController.m
//  Constellation
//
//  Created by Darren  xu on 16/7/1.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "SuperStarViewController.h"
#import "SuperStarCell.h"
#import "commendUserModel.h"
#import "SearchResultController.h"

@interface SuperStarViewController ()<UITabBarDelegate, UITableViewDataSource, UIScrollViewDelegate, UISearchBarDelegate>
@property (nonatomic, strong) NSMutableArray *userArr;
@property (nonatomic, strong) NSMutableArray *starNameArr;
@property (nonatomic, strong) UIAlertController *alert;
@property (nonatomic, assign) long maxID;

@end

@implementation SuperStarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_tab registerNib:[UINib nibWithNibName:@"SuperStarCell" bundle:nil] forCellReuseIdentifier:@"superStarCell"];
    
    // 上拉加载的请求参数初始值
    _tab.rowHeight = 61;
    _maxID = 1540;
    
    [self contentRequest];
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        NSLog(@"下拉刷新");
        // 重新请求数据————未做
        [self performSelector:@selector(refreshTableData) withObject:nil afterDelay:1.5];
    }];
    header.automaticallyChangeAlpha = YES;
    _tab.mj_header = header;
    
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        NSLog(@"上拉加载");
        //请求更多数据
        [self loadMoreData];
    }];
    footer.automaticallyChangeAlpha = YES;
    _tab.mj_footer = footer;

}

// 将状态栏文字设置为白色
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _userArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SuperStarCell *cell = [tableView dequeueReusableCellWithIdentifier:@"superStarCell" forIndexPath:indexPath];
    commendUserModel *user = _userArr[indexPath.row];
    
//    *headImg;
//    UIImageView *weiboImg;
//     UILabel *weiboName;
//     UILabel *starLabel
    
    cell.weiboName.text = user.weibo_name;
    cell.starLabel.text = self.starNameArr[[user.star intValue]-1];
    if (user.icon) {
        [cell.headImg setImageWithURL:[NSURL URLWithString:user.icon]];
    }else{
        cell.headImg.image = [UIImage imageNamed:@"bg_head_placeholder"];
    }
    if ([user.type isEqualToString:@"1"]) {
        cell.weiboImg.image = [UIImage imageNamed:@"weibo_sina"];
    }else{
        cell.weiboImg.image = [UIImage imageNamed:@"weibo_tx"];
    }
    
    return cell;
}

#pragma  mark - 请求方法

- (void)contentRequest{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:@"http://star21.mooker.cn/star/api/v3/getStarUsers.php?devid=rmlo%557F9J9F7TtN%31l%53%45spel%55yRdkJvFGl3Q6vqH%5AaPk%3D&type=2&ver=3.1&length=10&locale=0&appid=ff808081347ecaad013492cc2e5c0003&dev=iphone" parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        NSLog(@"加载进度————%.2f",downloadProgress.fractionCompleted);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"加载成功");
        NSArray *contentArr = responseObject[@"data"][@"users"];
        for (NSDictionary *dic in contentArr) {
            commendUserModel *user = [commendUserModel new];
            [user setValuesForKeysWithDictionary:dic[@"user"]];  // 用KVC为对象所有属性赋值
            [self.userArr addObject:user];
        }
        [_tab reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"加载失败 %@",error);
    }];
    
}

- (void)loadMoreDataRequest:(long)maxID{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSString *urlStr1 = @"http://star21.mooker.cn/star/api/v3/getStarUsers.php?devid=rmlo%557F9J9F7TtN%31l%53%45spel%55yRdkJvFGl3Q6vqH%5AaPk%3D&type=2&ver=3.1&length=10&locale=0&appid=ff808081347ecaad013492cc2e5c0003&";
    NSString *urlStr2 = [NSString stringWithFormat:@"max_id=%ld&dev=iphone",maxID];
    [manager GET:[NSString stringWithFormat:@"%@%@",urlStr1,urlStr2] parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        NSLog(@"加载进度————%.2f",downloadProgress.fractionCompleted);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"加载成功");
        //      [self.contentArr addObjectsFromArray:responseObject[@"data"][@"users"]];
        NSArray *additionArr = responseObject[@"data"][@"users"];
        for (NSDictionary *dic in additionArr) {
            commendUserModel *user = [commendUserModel new];
            [user setValuesForKeysWithDictionary:dic[@"user"]];
            [self.userArr addObject:user];
        }
        [self.tab.mj_footer endRefreshing];
        [_tab reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"加载失败 %@",error);
        [self.tab.mj_footer endRefreshing];
    }];
    
}

- (void)searchRequest{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSString *urlStr1 = @"http://star21.mooker.cn/star/api/v3/getStarUsers.php?devid=rmlo%557F9J9F7TtN%31l%53%45spel%55yRdkJvFGl3Q6vqH%5AaPk%3D&type=2&ver=3.1&star=0&length=30&locale=0&appid=ff808081347ecaad013492cc2e5c0003&keyword=";
    // 对中文参数进行UTF-8编码
    NSString *urlStr2 = [[NSString stringWithFormat:@"%@",_searchBar.text] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet controlCharacterSet]];
    NSString *urlStr3 = @"&dev=iphone";
    NSString *urlStrEncode = [NSString stringWithFormat:@"%@%@%@",urlStr1,urlStr2,urlStr3];
    NSLog(@"编码后的 url == %@ ",urlStrEncode);
    
    [manager GET:urlStrEncode parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        NSLog(@"加载进度————%.2f",downloadProgress.fractionCompleted);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"搜索成功");
        NSArray *contentArr = responseObject[@"data"][@"users"];
        NSMutableArray *tempArr = [[NSMutableArray alloc]init];
        for (NSDictionary *dic in contentArr) {
            commendUserModel *user = [commendUserModel new];
            [user setValuesForKeysWithDictionary:dic[@"user"]];  // 用KVC为对象所有属性赋值
            [tempArr addObject:user];
        }
        SearchResultController *resultVC = [[SearchResultController alloc]init];
        resultVC.resultArr = tempArr;
        [self.navigationController pushViewController:resultVC animated:YES];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"搜索失败 %@",error);
    }];
    
}

- (void)refreshTableData{
    [self.tab.mj_header endRefreshing];
    //    [_tab reloadData];
}

- (void)loadMoreData{
    _maxID -= 10;
    [self loadMoreDataRequest:_maxID];
}

- (IBAction)goBasck:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self searchRequest];
    [self.view endEditing:YES];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}

- (NSMutableArray *)userArr{
    if (_userArr == nil) {
        _userArr = [[NSMutableArray alloc]init];
    }
    return  _userArr;
}

- (NSMutableArray *)starNameArr{
    if (_starNameArr == nil) {
        _starNameArr = [[NSMutableArray alloc]init];
        [_starNameArr addObject:@"白羊座"];
        [_starNameArr addObject:@"金牛座"];
        [_starNameArr addObject:@"双子座"];
        [_starNameArr addObject:@"巨蟹座"];
        [_starNameArr addObject:@"狮子座"];
        [_starNameArr addObject:@"处女座"];
        [_starNameArr addObject:@"天秤座"];
        [_starNameArr addObject:@"天蝎座"];
        [_starNameArr addObject:@"射手座"];
        [_starNameArr addObject:@"魔蝎座"];
        [_starNameArr addObject:@"水瓶座"];
        [_starNameArr addObject:@"双鱼座"];
    }
    return _starNameArr;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
