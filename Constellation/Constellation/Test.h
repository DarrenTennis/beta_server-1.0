//
//  Test.h
//  Constellation
//
//  Created by Darren  xu on 16/6/28.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Test : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *leftArrow;
@property (nonatomic, assign) int starNumber;

- (IBAction)leftButton:(UIBarButtonItem *)sender;
- (IBAction)rightButton:(UIBarButtonItem *)sender;

@end
