//
//  OneTableViewCell.h
//  Constellation
//
//  Created by mac on 16/7/1.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OneTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *zhiShu;
@property (weak, nonatomic) IBOutlet UILabel *biZhong;

@end
