//
//  CommendViewController.m
//  Constellation
//
//  Created by Darren  xu on 16/6/30.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "CommendViewController.h"
#import "CommendCell.h"
#import "commendUserModel.h"
#import "SearchResultController.h"

@interface CommendViewController ()<UITabBarDelegate, UITableViewDataSource, UIScrollViewDelegate, UISearchBarDelegate>
@property (nonatomic, strong) NSMutableArray *userArr;
@property (nonatomic, strong) UIAlertController *alert;
@property (nonatomic, assign) long maxID;
@end

@implementation CommendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_tab registerNib:[UINib nibWithNibName:@"CommendCell" bundle:nil] forCellReuseIdentifier:@"commendCell"];
    
    // 上拉加载的请求参数初始值
    _tab.rowHeight = 61;
    _maxID = 60942;
    
    [self contentRequest];
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        NSLog(@"下拉刷新");
        // 重新请求数据————未做
        [self performSelector:@selector(refreshTableData) withObject:nil afterDelay:1.5];
    }];
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    header.automaticallyChangeAlpha = YES;
    _tab.mj_header = header;
    
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        NSLog(@"上拉加载");
        //请求更多数据
        [self loadMoreData];
    }];
    footer.automaticallyChangeAlpha = YES;
    _tab.mj_footer = footer;
}

// 将状态栏文字设置为白色
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _userArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CommendCell *cell = [tableView dequeueReusableCellWithIdentifier:@"commendCell" forIndexPath:indexPath];
    commendUserModel *user = _userArr[indexPath.row];
    
    cell.nameLab.text = user.nick_name;
    if (user.icon) {
        [cell.headImg setImageWithURL:[NSURL URLWithString:user.icon]];
    }else{
        cell.headImg.image = [UIImage imageNamed:@"bg_head_placeholder"];
    }
    if ([user.sex isEqualToString:@"1"]) {
        cell.sexImg.image = [UIImage imageNamed:@"bg_show_male"];
    }else{
        cell.sexImg.image = [UIImage imageNamed:@"bg_show_female"];
    }

    return cell;
}

#pragma  mark - 请求方法

- (void)contentRequest{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:@"http://star21.mooker.cn/star/api/v3/getStarUsers.php?devid=rmlo%557F9J9F7TtN%31l%53%45spel%55yRdkJvFGl3Q6vqH%5AaPk%3D&type=1&ver=3.1&length=10&locale=0&appid=ff808081347ecaad013492cc2e5c0003&dev=iphone" parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        NSLog(@"加载进度————%.2f",downloadProgress.fractionCompleted);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"加载成功");
        NSArray *contentArr = responseObject[@"data"][@"users"];
        for (NSDictionary *dic in contentArr) {
            commendUserModel *user = [commendUserModel new];
            [user setValuesForKeysWithDictionary:dic[@"user"]];  // 用KVC为对象所有属性赋值
            [self.userArr addObject:user];
        }
        [_tab reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"加载失败 %@",error);
    }];
    
}

- (void)loadMoreDataRequest:(long)maxID{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSString *urlStr1 = @"http://star21.mooker.cn/star/api/v3/getStarUsers.php?devid=rmlo%557F9J9F7TtN%31l%53%45spel%55yRdkJvFGl3Q6vqH%5AaPk%3D&type=1&ver=3.1&length=10&locale=0&appid=ff808081347ecaad013492cc2e5c0003&";
    NSString *urlStr2 = [NSString stringWithFormat:@"max_id=%ld&dev=iphone",maxID];
    [manager GET:[NSString stringWithFormat:@"%@%@",urlStr1,urlStr2] parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        NSLog(@"加载进度————%.2f",downloadProgress.fractionCompleted);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"加载成功");
//      [self.contentArr addObjectsFromArray:responseObject[@"data"][@"users"]];
        NSArray *additionArr = responseObject[@"data"][@"users"];
        for (NSDictionary *dic in additionArr) {
            commendUserModel *user = [commendUserModel new];
            [user setValuesForKeysWithDictionary:dic[@"user"]];
            [self.userArr addObject:user];
        }
        [self.tab.mj_footer endRefreshing];
        [_tab reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"加载失败 %@",error);
        [self.tab.mj_footer endRefreshing];
    }];
    
}

- (void)searchRequest{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSString *urlStr1 = @"http://star21.mooker.cn/star/api/v3/getStarUsers.php?devid=rmlo%557F9J9F7TtN%31l%53%45spel%55yRdkJvFGl3Q6vqH%5AaPk%3D&type=1&ver=3.1&star=0&length=30&locale=0&appid=ff808081347ecaad013492cc2e5c0003&keyword=";
    // 对中文参数进行UTF-8编码
    NSString *urlStr2 = [[NSString stringWithFormat:@"%@",_searchBar.text] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet controlCharacterSet]];
    NSString *urlStr3 = @"&dev=iphone";
    NSString *urlStrEncode = [NSString stringWithFormat:@"%@%@%@",urlStr1,urlStr2,urlStr3];
    NSLog(@"编码后的 url == %@ ",urlStrEncode);
    
    [manager GET:urlStrEncode parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        NSLog(@"加载进度————%.2f",downloadProgress.fractionCompleted);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"搜索成功");
        NSArray *contentArr = responseObject[@"data"][@"users"];
        NSMutableArray *tempArr = [[NSMutableArray alloc]init];
        for (NSDictionary *dic in contentArr) {
            commendUserModel *user = [commendUserModel new];
            [user setValuesForKeysWithDictionary:dic[@"user"]];  // 用KVC为对象所有属性赋值
            [tempArr addObject:user];
        }
        SearchResultController *resultVC = [[SearchResultController alloc]init];
        resultVC.resultArr = tempArr;
        [self.navigationController pushViewController:resultVC animated:YES];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"搜索失败 %@",error);
    }];
    
}

- (void)refreshTableData{
    [self.tab.mj_header endRefreshing];
//    [_tab reloadData];
}

- (void)loadMoreData{
    _maxID -= 10;
    [self loadMoreDataRequest:_maxID];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self searchRequest];
    [self.view endEditing:YES];
}

- (IBAction)leftBtn:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightBtn:(UIBarButtonItem *)sender {
    [self presentViewController:self.alert animated:YES completion:nil];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}

- (UIAlertController *)alert{
    if (_alert == nil) {
        _alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"只看女生" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"只看男生" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"查看全部" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [_alert addAction:action1];
        [_alert addAction:action2];
        [_alert addAction:action3];
        [_alert addAction:actionCancel];
    }
    return _alert;
}

- (NSMutableArray *)userArr{
    if (_userArr == nil) {
        _userArr = [[NSMutableArray alloc]init];
    }
    return  _userArr;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
