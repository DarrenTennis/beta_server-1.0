//
//  FateAnalyseViewCell.h
//  Constellation
//
//  Created by Darren  xu on 16/6/29.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FateAnalyseViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageBig;
@property (weak, nonatomic) IBOutlet UIImageView *image1;
@property (weak, nonatomic) IBOutlet UIImageView *image2;
@property (weak, nonatomic) IBOutlet UIImageView *image3;
@property (weak, nonatomic) IBOutlet UIImageView *image4;

@property (nonatomic, copy) void (^block)();

- (IBAction)detailBtn:(UIButton *)sender;

@end
