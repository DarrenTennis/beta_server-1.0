//
//  ViewController3.m
//  Constellation
//
//  Created by Darren  xu on 16/6/24.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "ViewController3.h"
#import "FriendViewCell.h"
#import "CommendViewController.h"
#import "SuperStarViewController.h"
#import "StarRankingViewController.h"
#import "PopularRankingViewController.h"

@interface ViewController3 ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray *groupNameArr;
@property (nonatomic, strong) UIImageView *headerImage;
@property (nonatomic, strong) UILabel *lab1;
@property (nonatomic, strong) UILabel *lab2;
@property (nonatomic, strong) UILabel *lab3;
@property (nonatomic, strong) UILabel *lab4;
@property (nonatomic, strong) UIButton *btn1;
@property (nonatomic, strong) UIButton *btn2;
@property (nonatomic, strong) UIButton *btn3;
@property (nonatomic, strong) UIButton *btn4;

@end

@implementation ViewController3

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_tab registerNib:[UINib nibWithNibName:@"FriendViewCell" bundle:nil] forCellReuseIdentifier:@"friendGroupCell"];
    _tab.tableHeaderView = self.headerImage;
    
    [self addConstrains];
}

// 将状态栏文字设置为白色
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 14;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FriendViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"friendGroupCell" forIndexPath:indexPath];
    cell.narrowImg.image = [UIImage imageNamed:@"bg_arrow_close"];
    cell.groupLab.text = self.groupNameArr[indexPath.row];
    cell.numLab.text = @"0";
    return cell;
}

- (void)addConstrains{
    
//    [_tab.tableHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(CGSizeMake(DEVICE_WIDTH, 240));
//        make.bottom.equalTo(@0);
//        make.leading.equalTo(@20);
//    }];
    
    [_headerImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(DEVICE_WIDTH-40, 180));
        make.top.equalTo(@0);
//      make.bottom.equalTo(_tab.tableHeaderView.mas_bottom).offset(-20);
        make.leading.equalTo(@20);
    }];
    
    [_lab1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(120, 25));
        make.leading.equalTo(@60);
        make.centerY.equalTo(_headerImage.mas_centerY).multipliedBy(0.25);
    }];
    
    [_lab2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(120, 25));
        make.leading.equalTo(@60);
        make.centerY.equalTo(_headerImage.mas_centerY).multipliedBy(0.75);
    }];

    [_lab3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(120, 25));
        make.leading.equalTo(@60);
        make.centerY.equalTo(_headerImage.mas_centerY).multipliedBy(1.25);
    }];

    [_lab4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(120, 25));
        make.leading.equalTo(@60);
        make.centerY.equalTo(_headerImage.mas_centerY).multipliedBy(1.75);
    }];

    [_btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(DEVICE_WIDTH-40, 45));
        make.centerY.equalTo(_headerImage.mas_centerY).multipliedBy(0.25);
        make.leading.equalTo(@0);
    }];
    
    [_btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(DEVICE_WIDTH-40, 45));
        make.centerY.equalTo(_headerImage.mas_centerY).multipliedBy(0.75);
        make.leading.equalTo(@0);
    }];

    [_btn3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(DEVICE_WIDTH-40, 45));
        make.centerY.equalTo(_headerImage.mas_centerY).multipliedBy(1.25);
        make.leading.equalTo(@0);
    }];

    [_btn4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(DEVICE_WIDTH-40, 45));
        make.centerY.equalTo(_headerImage.mas_centerY).multipliedBy(1.75);
        make.leading.equalTo(@0);
    }];

}

- (UIImageView *)headerImage{
    if (_headerImage == nil) {
        _headerImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_pk_menu"]];
        _headerImage.userInteractionEnabled = YES;
        _lab1 = [[UILabel alloc]init];
        _lab2 = [[UILabel alloc]init];
        _lab3 = [[UILabel alloc]init];
        _lab4 = [[UILabel alloc]init];
        _lab1.text = @"推荐星友";
        _lab1.textColor = [UIColor whiteColor];
        _lab2.text = @"星座明星";
        _lab2.textColor = [UIColor whiteColor];
        _lab3.text = @"星座排行榜";
        _lab3.textColor = [UIColor whiteColor];
        _lab4.text = @"人气排行榜";
        _lab4.textColor = [UIColor whiteColor];
        [_headerImage addSubview:_lab1];
        [_headerImage addSubview:_lab2];
        [_headerImage addSubview:_lab3];
        [_headerImage addSubview:_lab4];
        self.btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
        _btn1.tag = 1;
        _btn2.tag = 2;
        _btn3.tag = 3;
        _btn4.tag = 4;
        [_btn1 addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
        [_btn2 addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
        [_btn3 addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
        [_btn4 addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
        [_headerImage addSubview:_btn1];
        [_headerImage addSubview:_btn2];
        [_headerImage addSubview:_btn3];
        [_headerImage addSubview:_btn4];
    }
    return _headerImage;
}

- (NSMutableArray *)groupNameArr{
    if (_groupNameArr == nil) {
        _groupNameArr = [[NSMutableArray alloc]init];
        [_groupNameArr addObject:@"收藏星友"];
        [_groupNameArr addObject:@"未分组星友"];
        [_groupNameArr addObject:@"白羊座"];
        [_groupNameArr addObject:@"金牛座"];
        [_groupNameArr addObject:@"双子座"];
        [_groupNameArr addObject:@"巨蟹座"];
        [_groupNameArr addObject:@"狮子座"];
        [_groupNameArr addObject:@"处女座"];
        [_groupNameArr addObject:@"天秤座"];
        [_groupNameArr addObject:@"天蝎座"];
        [_groupNameArr addObject:@"射手座"];
        [_groupNameArr addObject:@"魔蝎座"];
        [_groupNameArr addObject:@"水瓶座"];
        [_groupNameArr addObject:@"双鱼座"];
    }
    return _groupNameArr;
}

- (void)clickButton:(UIButton *)sender{
    NSLog(@"点击Button %ld",sender.tag);
    switch (sender.tag) {
        case 1:
        {
            CommendViewController *vc = [[CommendViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:
        {
            SuperStarViewController *vc = [[SuperStarViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 3:
        {
            StarRankingViewController *vc = [[StarRankingViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 4:
        {
            PopularRankingViewController *vc = [[PopularRankingViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
