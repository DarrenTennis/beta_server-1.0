//
//  planisphereViewController.h
//  Constellation
//
//  Created by mac on 16/6/24.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface planisphereViewController : UIViewController
- (IBAction)returnBtn:(UIButton *)sender;
- (IBAction)selectConstellation:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *constellationLable;
@property (nonatomic,assign) NSInteger orSelect;
@end
