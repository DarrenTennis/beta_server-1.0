//
//  PartnershipViewController.m
//  Constellation
//
//  Created by mac on 16/6/23.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "PartnershipViewController.h"
#import "LovePairViewController.h"
#import "EatViewController.h"
#import "PhaseIndexViewController.h"
@interface PartnershipViewController ()

@end

@implementation PartnershipViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}
// 将状态栏文字设置为白色
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)LovePair:(UIButton *)sender {
    NSLog(@"111");
    LovePairViewController *lovePair=[[LovePairViewController alloc]init];
    [self.navigationController pushViewController:lovePair animated:YES];
}

- (IBAction)PhaseIndex:(UIButton *)sender {
    PhaseIndexViewController *phaseIndex=[[PhaseIndexViewController alloc]init];
    [self.navigationController pushViewController:phaseIndex animated:YES];
}

- (IBAction)eat:(UIButton *)sender {
    EatViewController *eat=[[EatViewController alloc]init];
    [self.navigationController pushViewController:eat animated:YES];
}
@end
