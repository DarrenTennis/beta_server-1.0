//
//  PartnershipViewController.h
//  Constellation
//
//  Created by mac on 16/6/23.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PartnershipViewController : UIViewController
- (IBAction)LovePair:(UIButton *)sender;
- (IBAction)PhaseIndex:(UIButton *)sender;
- (IBAction)eat:(UIButton *)sender;

@end
