//
//  EatViewController.m
//  Constellation
//
//  Created by mac on 16/6/27.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "EatViewController.h"

@interface EatViewController ()

@end

@implementation EatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    for (int i=0; i<12; i++) {
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(DEVICE_WIDTH/2-30, DEVICE_HEIGHT/2-50-160, 60, 320)];
        view.backgroundColor=[UIColor clearColor];
        [self.view addSubview:view];
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeSystem];
        btn.backgroundColor=[UIColor clearColor];
        [btn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"bt_chose_star%d",i+1]] forState:UIControlStateNormal];
        btn.frame=CGRectMake(0, 0, 60, 60);
        [view addSubview:btn];
        
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        view.transform=CGAffineTransformMakeRotation(M_PI/180*30*i);
        [UIView commitAnimations];
    }
}

// 将状态栏文字设置为白色
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)goBack:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
