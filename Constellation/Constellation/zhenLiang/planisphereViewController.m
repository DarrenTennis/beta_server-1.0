//
//  planisphereViewController.m
//  Constellation
//
//  Created by mac on 16/6/24.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "planisphereViewController.h"
#import "LovePairViewController.h"
@interface planisphereViewController ()
@property (nonatomic,retain) LovePairViewController *lovePair;
@property (nonatomic,retain) NSArray *constellationText;
@end

@implementation planisphereViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _constellationText=@[@"白羊座",@"金牛座",@"双子座",@"巨蟹座",@"狮子座",@"处女座",@"天秤座",@"天蝎座",@"射手座",@"摩羯座",@"水瓶座",@"双鱼座"];
}

// 将状态栏文字设置为白色
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)returnBtn:(UIButton *)sender {
   
    [self.navigationController popViewControllerAnimated:YES];

}
- (IBAction)selectConstellation:(UIButton *)sender {
    _lovePair=self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
    [self.navigationController popViewControllerAnimated:YES];
    switch (sender.tag) {
        case 1:
        if (_orSelect==1) {
            _lovePair.constellationBtn.titleLabel.text=_constellationText[0];
           
            }
        if (_orSelect==2) {
                _lovePair.constellationBtn2.titleLabel.text=_constellationText[0];
            }
            break;
        case 2:
        {
            if (_orSelect==1) {
                _lovePair.constellationBtn.titleLabel.text=_constellationText[1];
                
            }
            if (_orSelect==2) {
                _lovePair.constellationBtn2.titleLabel.text=_constellationText[1];
            }
            break;
        }
        case 3:
            if (_orSelect==1) {
                _lovePair.constellationBtn.titleLabel.text=_constellationText[2];
                
            }
            if (_orSelect==2) {
                _lovePair.constellationBtn2.titleLabel.text=_constellationText[2];
            }
            break;
        case 4:
            if (_orSelect==1) {
                _lovePair.constellationBtn.titleLabel.text=_constellationText[3];
                
            }
            if (_orSelect==2) {
                _lovePair.constellationBtn2.titleLabel.text=_constellationText[3];
            }
            break;
        case 5:
            if (_orSelect==1) {
                _lovePair.constellationBtn.titleLabel.text=_constellationText[4];
                
            }
            if (_orSelect==2) {
                _lovePair.constellationBtn2.titleLabel.text=_constellationText[4];
            }
            break;
        case 6:
            if (_orSelect==1) {
                _lovePair.constellationBtn.titleLabel.text=_constellationText[5];
                
            }
            if (_orSelect==2) {
                _lovePair.constellationBtn2.titleLabel.text=_constellationText[5];
            }
            break;
        case 7:
            if (_orSelect==1) {
                _lovePair.constellationBtn.titleLabel.text=_constellationText[6];
                
            }
            if (_orSelect==2) {
                _lovePair.constellationBtn2.titleLabel.text=_constellationText[6];
            }
            break;
        case 8:
            if (_orSelect==1) {
                _lovePair.constellationBtn.titleLabel.text=_constellationText[7];
                
            }
            if (_orSelect==2) {
                _lovePair.constellationBtn2.titleLabel.text=_constellationText[7];
            }
            break;
        case 9:
            if (_orSelect==1) {
                _lovePair.constellationBtn.titleLabel.text=_constellationText[8];
                
            }
            if (_orSelect==2) {
                _lovePair.constellationBtn2.titleLabel.text=_constellationText[8];
            }
            break;
        case 10:
            if (_orSelect==1) {
                _lovePair.constellationBtn.titleLabel.text=_constellationText[9];
                
            }
            if (_orSelect==2) {
                _lovePair.constellationBtn2.titleLabel.text=_constellationText[9];
            }
            break;
        case 11:
            if (_orSelect==1) {
                _lovePair.constellationBtn.titleLabel.text=_constellationText[10];
                
            }
            if (_orSelect==2) {
                _lovePair.constellationBtn2.titleLabel.text=_constellationText[10];
            }
            break;
        case 12:
            if (_orSelect==1) {
                _lovePair.constellationBtn.titleLabel.text=_constellationText[11];
                
            }
            if (_orSelect==2) {
                _lovePair.constellationBtn2.titleLabel.text=_constellationText[11];
            }
            break;
        default:
            break;
    }
    
    
    
}
@end
