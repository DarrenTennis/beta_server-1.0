//
//  LovePairViewController.h
//  Constellation
//
//  Created by mac on 16/6/24.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LovePairViewController : UIViewController
- (IBAction)selectplanisphere:(UIButton *)sender;
- (IBAction)start:(UIButton *)sender;
@property (strong, nonatomic)  UIButton *sexBtn, *sexBtn2;
@property (strong, nonatomic)  UIButton *constellationBtn, *constellationBtn2;
- (IBAction)returnBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn1;
@property (weak, nonatomic) IBOutlet UIImageView *bg_sex_maleImageview;
@property (weak, nonatomic) IBOutlet UIImageView *bg_sex_femaleImageview;

@property (weak, nonatomic) IBOutlet UIButton *selectBtn2;
@property (weak, nonatomic) IBOutlet UIImageView *bg_heartImageview;


@end
