//
//  LovePairViewController.m
//  Constellation
//
//  Created by mac on 16/6/24.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "LovePairViewController.h"
#import "planisphereViewController.h"
#import <AFNetworking.h>
#import "PairedResultViewController.h"
@interface LovePairViewController ()
@property (nonatomic,retain) UIButton *zhongjianbianliang;
@end

@implementation LovePairViewController

- (void)viewDidLoad {
    NSLog(@"宽是%@",NSStringFromCGRect(self.view.bounds));
    NSLog(@"宽是%@",NSStringFromCGRect(self.view.bounds));

    [super viewDidLoad];
    _constellationBtn=[UIButton buttonWithType:UIButtonTypeSystem];
    _sexBtn=[UIButton buttonWithType:UIButtonTypeSystem];
    [_constellationBtn setTitle:@"白羊座" forState:UIControlStateNormal];
    _constellationBtn.titleLabel.textColor=[UIColor blackColor];
    [_sexBtn setBackgroundImage:[UIImage imageNamed:@"bt_sex_male"] forState:UIControlStateNormal];
    [self.view addSubview:_constellationBtn];
    [self.view addSubview:_sexBtn];
    
    _bg_sex_maleImageview.sd_layout
    .leftSpaceToView(self.view, 20)
    .topSpaceToView(self.view, 140)
    .heightIs(60)
    .widthIs(120);
    _bg_sex_femaleImageview.sd_layout
    .rightSpaceToView(self.view, 20)
    .topSpaceToView(self.view, 140)
    .heightIs(60)
    .widthIs(120);
    
    _sexBtn.sd_layout
    .leftSpaceToView(self.view, 20)
    .topSpaceToView(self.view, 140)
    .heightIs(60)
    .widthIs(60);
    
    _constellationBtn.sd_layout
    .leftSpaceToView(_sexBtn, 0)
    .topEqualToView(_sexBtn)
    .heightRatioToView(_sexBtn, 1)
    .widthRatioToView(_sexBtn,1);
    [_constellationBtn addTarget:self action:@selector(changeSex:) forControlEvents:UIControlEventTouchUpInside];
    [_sexBtn addTarget:self action:@selector(changeSex:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _constellationBtn2=[UIButton buttonWithType:UIButtonTypeSystem];
    _sexBtn2=[UIButton buttonWithType:UIButtonTypeSystem];
    [_constellationBtn2 setTitle:@"白羊座" forState:UIControlStateNormal];
    _constellationBtn2.titleLabel.textColor=[UIColor blackColor];
    [_sexBtn2 setBackgroundImage:[UIImage imageNamed:@"bt_sex_female"] forState:UIControlStateNormal];
    [self.view addSubview:_constellationBtn2];
    [self.view addSubview:_sexBtn2];
    
    _sexBtn2.sd_layout
    .rightSpaceToView(self.view, 20)
    .topSpaceToView(self.view, 140)
    .heightIs(60)
    .widthIs(60);
    
    _bg_heartImageview.sd_layout
    .topSpaceToView(self.view, 160)
    .leftSpaceToView(self.view,[UIScreen mainScreen].bounds.size.width/2-10)
    .widthIs(20)
    .heightIs(20);
    NSLog(@"宽是%f",self.view.bounds.size.width);
    _constellationBtn2.sd_layout
    .rightSpaceToView(_sexBtn2, 0)
    .topEqualToView(_sexBtn2)
    .heightRatioToView(_sexBtn2, 1)
    .widthRatioToView(_sexBtn2,1);
    
    [_constellationBtn2 addTarget:self action:@selector(exchangeFrame:) forControlEvents:UIControlEventTouchUpInside];
    [_sexBtn2 addTarget:self action:@selector(exchangeFrame:) forControlEvents:UIControlEventTouchUpInside];
}

// 将状态栏文字设置为白色
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)selectplanisphere:(UIButton *)sender {
    planisphereViewController *planisphere=[[planisphereViewController alloc]init];
    planisphere.orSelect=sender.tag;
    [self.navigationController pushViewController:planisphere animated:YES];
}

- (IBAction)start:(UIButton *)sender {
    PairedResultViewController *pairedResult=[[PairedResultViewController alloc]init];
    [self.navigationController pushViewController:pairedResult animated:YES];
    
    
    
    
}

- (void)changeSex:(UIButton *)sender {
    sender.userInteractionEnabled=NO;
    //    sender.selected=!sender.selected;
    //变性
    static int i=0;
    i++;
    if (i%2==1) {
        [_sexBtn setBackgroundImage:[UIImage imageNamed:@"bt_sex_female"] forState:UIControlStateNormal];
    }
    if (i%2==0) {
        [_sexBtn setBackgroundImage:[UIImage imageNamed:@"bt_sex_male"] forState:UIControlStateNormal];
    }
    
    [UIView animateWithDuration:.5 animations:^{
        NSLog(@"切换前：%@____%@", NSStringFromCGRect(_sexBtn.frame), NSStringFromCGRect(_constellationBtn.frame));
        //切换位置
        CGRect b;
        b=_sexBtn.frame;
        _sexBtn.frame=_constellationBtn.frame;
        _constellationBtn.frame=b;
        NSLog(@"切换后：%@____%@", NSStringFromCGRect(_sexBtn.frame), NSStringFromCGRect(_constellationBtn.frame));
    } completion:^(BOOL finished) {
        sender.userInteractionEnabled = YES;
    }];
    
}
- (void)exchangeFrame:(UIButton *)sender {
    sender.userInteractionEnabled=NO;
    //    sender.selected=!sender.selected;
    //变性
    static int i=0;
    i++;
    if (i%2==1) {
        [_sexBtn2 setBackgroundImage:[UIImage imageNamed:@"bt_sex_male"] forState:UIControlStateNormal];
    }
    if (i%2==0) {
        [_sexBtn2 setBackgroundImage:[UIImage imageNamed:@"bt_sex_female"] forState:UIControlStateNormal];
    }
    
    [UIView animateWithDuration:.5 animations:^{
        NSLog(@"切换前：%@____%@", NSStringFromCGRect(_sexBtn2.frame), NSStringFromCGRect(_constellationBtn2.frame));
        //切换位置
        CGRect b;
        b=_sexBtn2.frame;
        _sexBtn2.frame=_constellationBtn2.frame;
        _constellationBtn2.frame=b;
        NSLog(@"切换后：%@____%@", NSStringFromCGRect(_sexBtn2.frame), NSStringFromCGRect(_constellationBtn2.frame));
    } completion:^(BOOL finished) {
        sender.userInteractionEnabled = YES;
    }];
    
}
- (IBAction)returnBtn:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
