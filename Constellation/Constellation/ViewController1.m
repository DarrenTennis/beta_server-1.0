//
//  ViewController1.m
//  Constellation
//
//  Created by Darren  xu on 16/6/24.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "ViewController1.h"
#import "FateAnalyseViewController.h"
#import "Test.h"

@interface ViewController1 ()
@property (nonatomic, strong) NSMutableArray *viewArray;
@property (nonatomic, assign) CGAffineTransform form;
@property (nonatomic, assign) float centerX;
@property (nonatomic, assign) float centerY;
@property (nonatomic, assign) float totalAngle;
@property (nonatomic, assign) int lastPointSection;
@property (nonatomic, assign) int flag;
@end

@implementation ViewController1

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.navigationItem.title = @"星座大师";
//    self.automaticallyAdjustsScrollViewInsets = NO;  // 关闭导航栏下方缩进64个像素
    
    _viewArray = [[NSMutableArray alloc]init];
    [_viewArray addObject:_view1];
    [_viewArray addObject:_view2];
    [_viewArray addObject:_view3];
    [_viewArray addObject:_view4];
    [_viewArray addObject:_view5];
    [_viewArray addObject:_view6];
    [_viewArray addObject:_view7];
    [_viewArray addObject:_view8];
    [_viewArray addObject:_view9];
    [_viewArray addObject:_view10];
    [_viewArray addObject:_view11];
    [_viewArray addObject:_view12];

    self.form = CGAffineTransformIdentity;
    for (int i=0; i<12; i++) {
        UIView *tempView = _viewArray[i];
        tempView.transform = CGAffineTransformRotate(self.form, M_PI/180*30*i);
    }
   
    self.centerX = [[UIScreen mainScreen]bounds].size.width/2.0;
    self.centerY = [[UIScreen mainScreen]bounds].size.height/2.0;
    NSLog(@"中心圆X坐标 = %.2f",_centerX);
    NSLog(@"中心圆Y坐标 = %.2f",_centerY);
    
}

// 将状态栏文字设置为白色
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch=[touches anyObject];
    float angle = 0;
    _flag = 888; // 表示移动过，防止没移动而只是单纯的touchesEnded跳转到下一界面

    if ([touch view] != self.view) {
        for (UIView *hitView in _viewArray) {
            if([touch view] == hitView){
                //手指滑动过程中的前一个点
//              CGPoint lastPoint = [touch previousLocationInView:hitView];    // 坐标系不一样
                CGPoint lastPoint = [touch previousLocationInView:self.view];
                
                //当前的点
//              CGPoint nowPoint = [touch locationInView:hitView];
                CGPoint nowPoint = [touch locationInView:self.view];
                
                //前后两点x和y的增量
                float dx = nowPoint.x - lastPoint.x;
//              float dy = nowPoint.y - lastPoint.y;
                
                //计算x、y与圆心的相对距离
                float dx1 = lastPoint.x - self.centerX;
                float dy1 = lastPoint.y - self.centerY;
                float dx2 = nowPoint.x - self.centerX;
                float dy2 = nowPoint.y - self.centerY;
                
                float c = sqrtf(dx1*dx1 + dy1*dy1)*sqrtf(dx2*dx2 + dy2*dy2);
                
                if (c != 0) {
                    angle = cosf((dx1*dx2 + dy1*dy2)/c);
                }
                // 判断旋转方向为顺时针还是逆时针,只需处理逆时针的情况,将旋转角度取反即可
                // X轴上方
                if (lastPoint.y <= _centerY && nowPoint.y <= _centerY && dx < 0) {
                    angle = -angle;
//                    NSLog(@"进入逆时针1");
                }
                // X轴下方
                else if (lastPoint.y >= _centerY && nowPoint.y >= _centerY && dx > 0) {
                    angle = -angle;
//                    NSLog(@"进入逆时针2");
                }
                // 临界点——X轴附近
                else if (lastPoint.y > _centerY && nowPoint.y < _centerY && nowPoint.x > _centerX) {
                    if (angle > 0) {
                        angle = -angle;
                    }
//                    NSLog(@"进入逆时针3");
                }
                // 临界点——X轴附近
                else if (lastPoint.y < _centerY && nowPoint.y > _centerY && nowPoint.x < _centerX) {
                    if (angle > 0) {
                        angle = -angle;
                    }
//                    NSLog(@"进入逆时针4");
                }
                
                for (int i=0; i<12; i++) {
                    UIView *tempView = _viewArray[i];
                    tempView.transform = CGAffineTransformRotate(tempView.transform, M_PI/180*angle*4);
                }
                _circle.transform = CGAffineTransformRotate(_circle.transform, M_PI/180*angle*4);
                _totalAngle += angle*4;
            }
        }
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    int pointSection = 0;
//求两个view的交集，判断指针所在区域，图片区域太大，有bug
//    for (int i=0; i<12; i++) {
//        UIView *tempView = _viewArray[i];
//        bool pointView = CGRectIntersectsRect(_point.frame, tempView.subviews);
//        if (pointView == YES) {
//            pointSection = i;
//            break;
//        }
//    }

    
//  计算旋转角度，判断指针所在区域
    int n = _totalAngle/360;
    _totalAngle = _totalAngle - (360 * n);
    
//  角度为正
    if (_totalAngle >= 0) {
        
        if(_totalAngle <= 15){
            pointSection = 1;
        }else{
            int m = (_totalAngle - 15)/30;
            m = m<11?10-m:m;
            pointSection = (m + 1)%12 + 1;
        }
//  角度为负
    }else{
        float angle = fabsf(_totalAngle);
        if(angle <= 15){
            pointSection = 1;
        }else{
            int m = (angle - 15)/30;
            pointSection = (m + 1)%12 + 1;
        }
    }
    
    UIView *pointView = _viewArray[pointSection-1];
    UIImageView *pointImage = pointView.subviews[0];
    pointImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"bt_selected_%d",pointSection]];
    
    if (_lastPointSection != 0) {
        UIView *lastPointView = _viewArray[_lastPointSection-1];
        UIImageView *lastPointImage = lastPointView.subviews[0];
        lastPointImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"bt_%d",_lastPointSection]];
    }

    NSLog(@"选中%d", pointSection);
    _lastPointSection = pointSection;
    
    if (_flag == 888) {
        [self performSelector:@selector(goNext) withObject:nil afterDelay:0.5];
        _flag = 0;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)goNext{
    Test *fateAnalyse = [[Test alloc]init];
    fateAnalyse.starNumber = _lastPointSection;
    [self.navigationController pushViewController:fateAnalyse animated:YES];
}
//- (IBAction)testbutton:(id)sender {
//    NSLog(@"点击测试按钮");
//    FateAnalyseViewController *fateAnalyse = [[FateAnalyseViewController alloc]init];
//    [self.navigationController pushViewController:fateAnalyse animated:YES];
//}
//
//- (IBAction)test2:(id)sender {
//    NSLog(@"点击测试按钮2");
//    Test *fateAnalyse = [[Test alloc]init];
//    [self.navigationController pushViewController:fateAnalyse animated:YES];
//
//}

@end
