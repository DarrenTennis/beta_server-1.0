//
//  SuperStarCell.h
//  Constellation
//
//  Created by Darren  xu on 16/7/1.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuperStarCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UIImageView *weiboImg;
@property (weak, nonatomic) IBOutlet UILabel *weiboName;
@property (weak, nonatomic) IBOutlet UILabel *starLabel;

@end
