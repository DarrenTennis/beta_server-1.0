//
//  FateAnalyseViewController.m
//  Constellation
//
//  Created by Darren  xu on 16/6/27.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "FateAnalyseViewController.h"
#import "ContentViewController.h"

@interface FateAnalyseViewController ()<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *topScroll;
@property (nonatomic, strong) UIScrollView *bottomScroll;
@property (nonatomic, strong) UIView *topScrollContent;
@property (nonatomic, strong) UIView *bottomScrollContent;
@property (nonatomic, strong) NSMutableArray *labelArr;
@property (nonatomic, strong) NSMutableArray *contentArr;
           
@end

@implementation FateAnalyseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addSubviews];
    [self addConstrains];
    [self contentRequest];

}

- (void)viewDidAppear:(BOOL)animated{
    NSLog(@"contentSize:%@", NSStringFromCGSize(self.topScroll.contentSize));
    NSLog(@"topScroll frame:%@", NSStringFromCGRect(self.topScroll.frame));
    for (UILabel *lab in _labelArr) {
        NSLog(@"label frame:%@", NSStringFromCGRect(lab.frame));
    }
}

// 将状态栏文字设置为白色
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)addSubviews{
    
    // 将左箭头旋转180°
    _leftArrow.transform = CGAffineTransformMakeRotation(M_PI);
    
    [self.view addSubview:self.topScroll];
    [self.topScroll addSubview:self.topScrollContent];
    
    [self.view addSubview:self.bottomScroll];
    [self.bottomScroll addSubview:self.bottomScrollContent];
    
    
    for (int i=0; i<7; i++) {
        UILabel *lab = [[UILabel alloc]init];
        lab.backgroundColor = [UIColor colorWithRed:arc4random()%256/255.0 green:arc4random()%256/255.0 blue:arc4random()%256/255.0 alpha:0.7];
        lab.text = @"5555";
        lab.textColor = [UIColor whiteColor];
        [self.labelArr addObject:lab];
        [self.topScrollContent addSubview:lab];
    }
    
    for (int i=0; i<7; i++) {
        ContentViewController *contenVC = [[ContentViewController alloc]init];
        [self addChildViewController:contenVC];
        [self.bottomScrollContent addSubview:contenVC.view];
    }
}

- (void)addConstrains{
    
    // 顶部的scroll View
    [_topScroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@64);
        make.height.equalTo(@44);
        make.leading.equalTo(self.view);
        make.trailing.equalTo(self.view);
    }];
    
    // scrollView content
    [_topScrollContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.topScroll);
        make.height.equalTo(self.topScroll);
    }];
    
    
    // 并排的7个label
    UILabel *lastLab = nil;
    for (int i=0; i<7; i++) {
        UILabel *tempLab = self.labelArr[i];
        [tempLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.topScrollContent);
            make.bottom.equalTo(self.topScrollContent);
            make.width.equalTo(self.topScroll.mas_width).multipliedBy(1.0/3);
            make.leading.equalTo(lastLab?lastLab.mas_trailing:@0);
        }];
        lastLab = tempLab;
    }
    
    // 因为需要横向滑动，所以需设置contentView的宽约束，将宽设置成实际需要的大小，从而将scrollView的宽撑起
    [_topScrollContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(lastLab.mas_trailing);
    }];
    
    
    // 底部的scroll View
    [_bottomScroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_topScroll.mas_bottom);
        make.leading.equalTo(self.view);
        make.trailing.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    [_bottomScrollContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.bottomScroll);
        make.height.equalTo(self.bottomScroll);
    }];
    
//   并排的7个子视图控制器(控制器内放table)
    ContentViewController *lastVC = nil;
    for (int i=0; i<7; i++) {
        ContentViewController *currenVC = self.childViewControllers[i];
        [currenVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.bottomScrollContent.mas_top);
            make.bottom.equalTo(self.bottomScrollContent.mas_bottom);
            make.width.equalTo(self.bottomScroll.mas_width);
            make.leading.equalTo(lastVC?lastVC.view.mas_trailing:@0);
        }];
        lastVC = currenVC;
        NSLog(@"currenVC frame == %@",[NSValue valueWithCGRect:currenVC.view.frame]);
    }
    
//   因为需要横向滑动，所以需设置contentView的宽约束，将宽设置成实际需要的大小，从而将scrollView的宽撑起
    [_bottomScrollContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(lastVC.view.mas_trailing);
    }];
    
}

- (void)contentRequest{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
    // 上传图片后相应的数据responseObject变成了NSData类型(原本为NSDictionary),需设置相应序列化为AFJSONResponseSerializer
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:@"http://localhost:8080/st/news/news_list.json" parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        NSLog(@"加载进度————%.2f",downloadProgress.fractionCompleted);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"加载成功");
//        self.newsArr = [NSMutableArray arrayWithArray:responseObject[@"news_list"]];
//        [_tab reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"加载失败 %@",error);
    }];

}


#pragma mark - Getter
- (UIScrollView *)topScroll{
    
    if (_topScroll == nil) {
        _topScroll = [[UIScrollView alloc]init];
        _topScroll.pagingEnabled = YES;
        _topScroll.delegate = self;
    }
    return _topScroll;
}

- (UIView *)topScrollContent{
    if (_topScrollContent == nil) {
        _topScrollContent = [UIView new];
    }
    return _topScrollContent;
}

- (UIScrollView *)bottomScroll{
    
    if (_bottomScroll == nil) {
        _bottomScroll = [[UIScrollView alloc]init];
        _bottomScroll.pagingEnabled = YES;
        _bottomScroll.delegate = self;
    }
    return _bottomScroll;
}

- (UIView *)bottomScrollContent{
    if (_bottomScrollContent == nil) {
        _bottomScrollContent = [[UIView alloc]init];
    }
    return _bottomScrollContent;
}

- (NSMutableArray *)labelArr{
    if (_labelArr == nil) {
        _labelArr = [[NSMutableArray alloc]init];
    }
    return _labelArr;
}

- (NSMutableArray *)contentArr{
    if (_contentArr == nil) {
        _contentArr = [[NSMutableArray alloc]init];
    }
    return _contentArr;
}


#pragma mark - 按钮方法
- (IBAction)goBack:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)share:(UIBarButtonItem *)sender {
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
