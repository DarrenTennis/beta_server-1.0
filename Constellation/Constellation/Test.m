//
//  Test.m
//  Constellation
//
//  Created by Darren  xu on 16/6/28.
//  Copyright © 2016年 Darren  xu. All rights reserved.
//

#import "Test.h"
#import "ContentViewController.h"
@interface Test ()<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *topScroll;
@property (nonatomic, strong) UIScrollView *bottomScroll;
@property (nonatomic, strong) UIView *topScrollContent;
@property (nonatomic, strong) UIView *bottomScrollContent;
@property (nonatomic, strong) UIImageView *bgImage;
@property (nonatomic, strong) NSMutableArray *labelArr;
@property (nonatomic, strong) NSMutableArray *labelTextArr;
@property (nonatomic, strong) NSMutableArray *contentArr;
@property (nonatomic, assign) float currenOffset;

@end

@implementation Test

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _bgImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_common_view"]];
    
    [self addSubviews];
    [self addConstrains];
    [self contentRequest];
}


- (void)viewDidAppear:(BOOL)animated{
    // 设置偏移量
    self.topScroll.contentOffset = CGPointMake(DEVICE_WIDTH*2/3, 0);
    self.bottomScroll.contentOffset = CGPointMake(DEVICE_WIDTH*3, 0);
    self.currenOffset = _bottomScroll.contentOffset.x;
}

// 将状态栏文字设置为白色
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


- (void)addSubviews{
    
    _leftArrow.transform = CGAffineTransformMakeRotation(M_PI);
    [self.view addSubview:self.bgImage];
    
    [self.view addSubview:self.bottomScroll];
    [self.bottomScroll addSubview:self.bottomScrollContent];
    [self.view addSubview:self.topScroll];
    [self.topScroll addSubview:self.topScrollContent];

    for (int i=0; i<7; i++) {
        UILabel *lab = [[UILabel alloc]init];
        lab.text = self.labelTextArr[i];
        lab.textColor = [UIColor lightGrayColor];
        lab.textAlignment = NSTextAlignmentCenter;
        [self.labelArr addObject:lab];
        [self.topScrollContent addSubview:lab];
    }

    for (int i=0; i<7; i++) {
        ContentViewController *contenVC = [[ContentViewController alloc]init];
        [self addChildViewController:contenVC];
        [self.bottomScrollContent addSubview:contenVC.view];
    }
}

- (void)addConstrains{
    [_bgImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    // 顶部的scroll View
    [_topScroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@64);
        make.height.equalTo(@44);
        make.leading.equalTo(self.view);
        make.trailing.equalTo(self.view);
    }];
    
    // scrollView content
    [_topScrollContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.topScroll);
        make.height.equalTo(self.topScroll);
    }];
    
    
    // 并排的7个label
    UILabel *lastLab = nil;
    for (int i=0; i<7; i++) {
        UILabel *tempLab = self.labelArr[i];
        [tempLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.topScrollContent);
            make.bottom.equalTo(self.topScrollContent);
            make.width.equalTo(self.topScroll.mas_width).multipliedBy(1.0/3);
            make.leading.equalTo(lastLab?lastLab.mas_trailing:@0);
        }];
        lastLab = tempLab;
    }
    
    // 因为需要横向滑动，所以需设置contentView的宽约束，将宽设置成实际需要的大小，从而将scrollView的宽撑起
    [_topScrollContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(lastLab.mas_trailing);
    }];
    
    // 底部的scroll View
    [_bottomScroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_topScroll.mas_bottom);
        make.leading.equalTo(self.view);
        make.trailing.equalTo(self.view);
        make.bottom.equalTo(self.view);
        
    }];
    // scrollView content
    [_bottomScrollContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.bottomScroll);
        make.height.equalTo(self.bottomScroll);

    }];
    
    // 并排的7个子视图控制器(控制器内放table)
    ContentViewController *lastVC = nil;
    for (int i=0; i<7; i++) {
        ContentViewController *currenVC = self.childViewControllers[i];
        [currenVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.bottomScrollContent.mas_top);
            make.bottom.equalTo(self.bottomScrollContent.mas_bottom);
            make.width.equalTo(self.bottomScroll.mas_width);
            make.leading.equalTo(lastVC?lastVC.view.mas_trailing:@0);
        }];
        lastVC = currenVC;
    }
    
    // 因为需要横向滑动，所以需设置contentView的宽约束，将宽设置成实际需要的大小，从而将scrollView的宽撑起
    [_bottomScrollContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(lastVC.view.mas_trailing);
    }];
}

- (void)contentRequest{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSString *str1 = @"http://star21.mooker.cn/star/api/v2/getStarTrend.php?app=ff808081347ecaad013492cc2e5c0003&dev=iphone&locale=0&";
    NSString *str2 = [NSString stringWithFormat:@"star=%d&ver=3.1",self.starNumber];
    [manager GET:[NSString stringWithFormat:@"%@%@",str1,str2] parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        NSLog(@"加载进度————%.2f",downloadProgress.fractionCompleted);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"加载成功");
//      NSLog(@"%@",responseObject);
        [self setFateContent:responseObject];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"加载失败 %@",error);
    }];
    
}

#pragma mark - Getter方法
- (UIScrollView *)topScroll{
    
    if (_topScroll == nil) {
        _topScroll = [[UIScrollView alloc]init];
        _topScroll.pagingEnabled = YES;
        _topScroll.delegate = self;
        _topScroll.tag = 999;
        _topScroll.scrollEnabled = NO;
    }
    return _topScroll;
}

- (UIView *)topScrollContent{
    if (_topScrollContent == nil) {
        _topScrollContent = [UIView new];
    }
    return _topScrollContent;
}

- (UIScrollView *)bottomScroll{
    
    if (_bottomScroll == nil) {
        _bottomScroll = [[UIScrollView alloc]init];
        _bottomScroll.pagingEnabled = YES;
        _bottomScroll.delegate = self;
        _bottomScroll.tag = 888;
        
    }
    return _bottomScroll;
}

- (UIView *)bottomScrollContent{
    if (_bottomScrollContent == nil) {
        _bottomScrollContent = [[UIView alloc]init];
    }
    return _bottomScrollContent;
}

- (NSMutableArray *)labelArr{
    if (_labelArr == nil) {
        _labelArr = [[NSMutableArray alloc]init];
    }
    return _labelArr;
}

- (NSMutableArray *)contentArr{
    if (_contentArr == nil) {
        _contentArr = [[NSMutableArray alloc]init];
    }
    return _contentArr;
}

- (NSMutableArray *)labelTextArr{
    if (_labelTextArr == nil) {
        _labelTextArr = [[NSMutableArray alloc]init];
        [_labelTextArr addObject:@"本周运势"];
        [_labelTextArr addObject:@"本月运势"];
        [_labelTextArr addObject:@"本年运势"];
        [_labelTextArr addObject:@"今日运势"];
        [_labelTextArr addObject:@"明日运势"];
        [_labelTextArr addObject:@"本周运势"];
        [_labelTextArr addObject:@"本月运势"];
    }
    return _labelTextArr;
}


#pragma mark - 协议方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //FIXME: 重新设置边界条件
    if (scrollView.tag == 888) {
        float offestX = scrollView.contentOffset.x;
        if(offestX <= 0){
            scrollView.contentOffset = CGPointMake(5*375+offestX, 0);
        }
        if(offestX >= 6*375){
            scrollView.contentOffset = CGPointMake(375+(offestX-6*375), 0);
        }
    }
    if (scrollView.tag == 999) {
        float offestX = scrollView.contentOffset.x;
        if(offestX <= 0){
            scrollView.contentOffset = CGPointMake(5*125+offestX, 0);
        }
        if(offestX >= 6*125){
            scrollView.contentOffset = CGPointMake(125+(offestX-6*125), 0);
        }
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    if (_bottomScroll.contentOffset.x > _currenOffset) {
        [UIView animateWithDuration:.5 animations:^{
            _topScroll.contentOffset = CGPointMake(_topScroll.contentOffset.x+125, 0);
        }];
    }else{
        [UIView animateWithDuration:.5 animations:^{
            _topScroll.contentOffset = CGPointMake(_topScroll.contentOffset.x-125, 0);
        }];
    }
    _currenOffset = _bottomScroll.contentOffset.x;
}

#pragma mark - 按钮方法

- (IBAction)leftButton:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightButton:(UIBarButtonItem *)sender {
}

- (void)setFateContent:(NSDictionary *)response{
    
    NSDictionary *dic = response[@"data"][0][@"trend"];
    
    ContentViewController *vc1 = self.childViewControllers[0];
    vc1.contentDic = dic[@"week"];
    vc1.starNum = dic[@"star"];
    [vc1 refreshTableData];
    ContentViewController *vc2 = self.childViewControllers[1];
    vc2.contentDic = dic[@"month"];
    vc2.starNum = dic[@"star"];
    [vc2 refreshTableData];
    ContentViewController *vc3 = self.childViewControllers[2];
    vc3.contentDic = dic[@"year"];
    vc3.starNum = dic[@"star"];
    [vc3 refreshTableData];
    ContentViewController *vc4 = self.childViewControllers[3];
    vc4.contentDic = dic[@"today"];
    vc4.starNum = dic[@"star"];
    [vc4 refreshTableData];
    ContentViewController *vc5 = self.childViewControllers[4];
    vc5.contentDic = dic[@"tomorrow"];
    vc5.starNum = dic[@"star"];
    [vc5 refreshTableData];
    ContentViewController *vc6 = self.childViewControllers[5];
    vc6.contentDic = dic[@"week"];
    vc6.starNum = dic[@"star"];
    [vc6 refreshTableData];
    ContentViewController *vc7 = self.childViewControllers[6];
    vc7.contentDic = dic[@"month"];
    vc7.starNum = dic[@"star"];
    [vc7 refreshTableData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
